import database.*;
import helpers.AuthorizationService;
import objects.AttendedCourse;
import objects.User;
import objects.UserRole;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

class AbstractTest {

    protected static CoursesDao coursesDao;
    protected static AttendedCoursesDao attendedCoursesDao;
    protected static CreditDao creditDao;
    protected static UserDao userDao;
    protected static UserRoleDao userRoleDao;

    protected static final String JSON_COURSES = "TEST_COURSE.json";
    protected static final String JSON_ATTENDED = "TEST_ATTENDED.json";
    protected static final String JSON_CREDIT = "TEST_CREDIT.json";
    protected static final String JSON_USER = "TEST_USER.json";
    protected static final String JSON_USER_ROLE = "TEST_ROLE.json";

    protected static final String USER_PASS = "password";

    @Before
    public void setUp() throws Exception {
        coursesDao = new CoursesDao(JSON_COURSES);
        attendedCoursesDao = new AttendedCoursesDao(JSON_ATTENDED);
        creditDao = new CreditDao(JSON_CREDIT);
        userDao = new UserDao(JSON_USER);
        userRoleDao = new UserRoleDao(JSON_USER_ROLE);
    }

    protected User createUser(Long roleId) {
        User user = new User();
        user.setFirstname("firstname");
        user.setLastname("lastName");
        user.setRole(roleId);
        user.setEmail("email");
        user.setActive(true);
        user.setPass(AuthorizationService.encrypt(USER_PASS));
        return new User(userDao.createObject(user));
    }

    protected UserRole createRole(Long id) {
        UserRole userRole = new UserRole();
        userRole.setName("role");
        userRole.setDescription("desc");
        userRole.setId(id);
        return new UserRole(userRoleDao.createObject(userRole));
    }


}
