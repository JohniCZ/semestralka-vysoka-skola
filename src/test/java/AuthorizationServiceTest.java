import helpers.AuthorizationService;
import objects.AuthorizationContext;
import objects.User;
import objects.UserRole;
import org.junit.*;

import java.io.File;

public class AuthorizationServiceTest extends AbstractTest {

    private User user;
    private UserRole userRole;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        user = createUser(1L);
        userRole = createRole(1L);
    }

    @After
    public void tearDown() throws Exception {
        try {
            File f = new File(JSON_USER);
            f.delete();
            f = new File(JSON_USER_ROLE);
            f.delete();
        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }

    @Ignore("Now Untesteble")
    @Test
    public void testHdsAuthorized() {
        AuthorizationContext authorizationContext = AuthorizationService.authorize(user.getEmail(), USER_PASS);
        Assert.assertNotNull(authorizationContext);
        Assert.assertEquals(user.getId(), authorizationContext.getUser().getId());
        Assert.assertEquals(userRole.getId(), authorizationContext.getUserRole().getId());
    }

    @Test
    public void testHdsNotAuthorized() {
        AuthorizationContext authorizationContext = AuthorizationService.authorize(user.getEmail(), "INVALID_PASS");
        Assert.assertNull(authorizationContext);

        authorizationContext = AuthorizationService.authorize("INVALID_MAIL", USER_PASS);
        Assert.assertNull(authorizationContext);
    }

    @Test
    public void testEncryption() {
        String encrypted = AuthorizationService.encrypt(USER_PASS);
        Assert.assertNotEquals(USER_PASS, encrypted);
    }
}
