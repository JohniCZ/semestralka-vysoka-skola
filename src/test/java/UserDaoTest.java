import objects.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.Date;

public class UserDaoTest extends AbstractTest {

    @After
    public void tearDown() throws Exception {
        try {
            File f = new File(JSON_USER);
            f.delete();
        } catch (
                Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testHds() {
        User user = new User(userDao.createObject(new User(1L, "bsda", "dsadsa", "dasda", "dsada", new Date(), new Date(), new Date(), true)));
        Assert.assertNotNull(user);
    }
}
