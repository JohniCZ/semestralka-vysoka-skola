package objects;

import java.util.Date;

public class AttendedCourse {
    private Long id;
    private Long userId;
    private Long courseId;
    private Long type;
    private Date dateCreated;

    public AttendedCourse() {
    }

    public AttendedCourse(Long id, Long userId, Long courseId, Long type, Date dateCreated) {
        this.id = id;
        this.userId = userId;
        this.courseId = courseId;
        this.type = type;
        this.dateCreated = dateCreated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
