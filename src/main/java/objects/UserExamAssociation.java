package objects;

import java.util.Date;

public class UserExamAssociation {
    private int id;
    private int userId;
    private int examId;
    private int status;
    private int result;
    private Date dateCreated;
    private Date dateModified;

    public UserExamAssociation(int id, int userId, int examId, int status, int result, Date dateCreated, Date dateModified) {
        this.id = id;
        this.userId = userId;
        this.examId = examId;
        this.status = status;
        this.result = result;
        this.dateCreated = dateCreated;
        this.dateModified = dateModified;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getExamId() {
        return examId;
    }

    public void setExamId(int examId) {
        this.examId = examId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }
}
