package objects;

import java.util.Date;

public class Exam {
    private int id;
    private int ownerId;
    private Date createdDate;

    public Exam(int id, int ownerId, Date createdDate) {
        this.id = id;
        this.ownerId = ownerId;
        this.createdDate = createdDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
