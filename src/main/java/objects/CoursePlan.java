package objects;

import java.util.Date;

public class CoursePlan {
    private int id;
    private String name;
    private int authorId;
    private Date dateCreated;

    public CoursePlan(int id, String name, int authorId, Date dateCreated) {
        this.id = id;
        this.name = name;
        this.authorId = authorId;
        this.dateCreated = dateCreated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
