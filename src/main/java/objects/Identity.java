package objects;

public class Identity {
    private User identity;
    private boolean isAuthorized;

    public Identity(User identity, boolean isAuthorized) {
        this.identity = identity;
        this.isAuthorized = isAuthorized;
    }

    public User getIdentity() {
        return identity;
    }

    public void setIdentity(User identity) {
        this.identity = identity;
    }

    public boolean isAuthorized() {
        return isAuthorized;
    }

    public void setAuthorized(boolean authorized) {
        isAuthorized = authorized;
    }
}
