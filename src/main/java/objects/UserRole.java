package objects;

import org.json.simple.JSONObject;

import java.util.Date;

public class UserRole {
    private Long id;
    private String name;
    private String description;
    private Date dateCreated;

    public UserRole() {
    }

    public UserRole(Long id, String name, String description, Date dateCreated) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.dateCreated = dateCreated;
    }

    public UserRole(JSONObject jsonObject) {
        this.id = (Long) jsonObject.get("id");
        this.name = (String) jsonObject.get("name");
        this.description = (String) jsonObject.get("description");
        this.dateCreated = (Date) jsonObject.get("dateCreated");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
