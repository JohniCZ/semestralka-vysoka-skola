package objects;

import org.json.simple.JSONObject;

import java.util.Date;

public class User {
    private Long id;
    private String firstname;
    private String lastname;
    private String email;
    private String pass;
    private Date dateCreated;
    private Date dateLastLogin;
    private Date dateLastAction;
    private boolean isActive;
    private Long role; //default value

    public User() {
    }

    public User(Long id, String firstname, String lastname, String email, String pass, Date dateCreated, Date dateLastLogin, Date dateLastAction, boolean isActive) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.pass = pass;
        this.dateCreated = dateCreated;
        this.dateLastLogin = dateLastLogin;
        this.dateLastAction = dateLastAction;
        this.isActive = isActive;
    }

    public User(JSONObject jsonObject) {
        this.id = (Long) jsonObject.get("id");
        this.firstname = (String) jsonObject.get("firstname");
        this.lastname = (String) jsonObject.get("lastname");
        this.email = (String) jsonObject.get("email");
        this.pass = (String) jsonObject.get("pass");
        this.dateCreated = (Date) jsonObject.get("dateCreated");
        this.dateLastLogin = (Date) jsonObject.get("dateLastLogin");
        this.dateLastAction = (Date) jsonObject.get("dateLastAction");
        this.isActive = (boolean) jsonObject.get("isActive");
        this.role = (Long) jsonObject.get("role");

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateLastLogin() {
        return dateLastLogin;
    }

    public void setDateLastLogin(Date dateLastLogin) {
        this.dateLastLogin = dateLastLogin;
    }

    public Date getDateLastAction() {
        return dateLastAction;
    }

    public void setDateLastAction(Date dateLastAction) {
        this.dateLastAction = dateLastAction;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public Long getRole() {
        return role;
    }

    public void setRole(Long role) {
        this.role = role;
    }
}
