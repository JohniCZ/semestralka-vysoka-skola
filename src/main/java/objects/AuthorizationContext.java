package objects;

public class AuthorizationContext {

    private User user;

    private UserRole userRole;

    public AuthorizationContext(User user, UserRole userRole) {
        this.user = user;
        this.userRole = userRole;
    }

    public AuthorizationContext() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }
}
