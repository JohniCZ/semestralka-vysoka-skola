package objects;

import java.util.Date;

public class BelongsToCoursePlan {
    private int id;
    private int courseId;
    private int coursePlanId;
    private Date dateCreated;
    private boolean isMandatory;

    public BelongsToCoursePlan(int id, int courseId, int coursePlanId, Date dateCreated, boolean isMandatory) {
        this.id = id;
        this.courseId = courseId;
        this.coursePlanId = coursePlanId;
        this.dateCreated = dateCreated;
        this.isMandatory = isMandatory;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public int getCoursePlanId() {
        return coursePlanId;
    }

    public void setCoursePlanId(int coursePlanId) {
        this.coursePlanId = coursePlanId;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean isMandatory() {
        return isMandatory;
    }

    public void setMandatory(boolean mandatory) {
        isMandatory = mandatory;
    }
}
