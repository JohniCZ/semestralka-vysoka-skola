package objects;

import org.json.simple.JSONObject;

import java.util.Date;

public class Course {
    private Long id;
    private String name;
    private String description;
    private Long creditsValue;
    private Long recommendedSemester;
    private Date dateCreated;

    public Course(String name, String description, Long creditsValue, Long recommendedSemester, Date dateCreated) {
        this.name = name;
        this.description = description;
        this.creditsValue = creditsValue;
        this.recommendedSemester = recommendedSemester;
        this.dateCreated = dateCreated;
    }

    public Course(Long id, String name, String description, Long creditsValue, Long recommendedSemester, Date dateCreated) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.creditsValue = creditsValue;
        this.recommendedSemester = recommendedSemester;
        this.dateCreated = dateCreated;
    }

    public Course(JSONObject jsonObject) {
        this.id = (Long) jsonObject.get("id");
        this.name = (String) jsonObject.get("name");
        this.description = (String) jsonObject.get("description");
        this.creditsValue = (Long) jsonObject.get("creditsValue");
        this.recommendedSemester = (Long) jsonObject.get("recommendedSemester");
        this.dateCreated = (Date) jsonObject.get("dateCreated");

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCreditsValue() {
        return creditsValue;
    }

    public void setCreditsValue(Long creditsValue) {
        this.creditsValue = creditsValue;
    }

    public Long getRecommendedSemester() {
        return recommendedSemester;
    }

    public void setRecommendedSemester(Long recommendedSemester) {
        this.recommendedSemester = recommendedSemester;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
