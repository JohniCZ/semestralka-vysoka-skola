package objects;

import java.util.Date;

public class BelongsToRole {
    private int id;
    private int userId;
    private int userRoleId;
    private Date dateCreated;

    public BelongsToRole(int id, int userId, int userRoleId, Date dateCreated) {
        this.id = id;
        this.userId = userId;
        this.userRoleId = userRoleId;
        this.dateCreated = dateCreated;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(int userRoleId) {
        this.userRoleId = userRoleId;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
