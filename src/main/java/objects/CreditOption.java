package objects;

import java.util.Date;

public class CreditOption {
    private Long id;
    private int userId;
    private int creditsValue;
    private int reasonType;
    private int state;
    private Date dateCreated;

    public CreditOption(Long id, int userId, int creditsValue, int reasonType, int state, Date dateCreated) {
        this.id = id;
        this.userId = userId;
        this.creditsValue = creditsValue;
        this.reasonType = reasonType;
        this.state = state;
        this.dateCreated = dateCreated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCreditsValue() {
        return creditsValue;
    }

    public void setCreditsValue(int creditsValue) {
        this.creditsValue = creditsValue;
    }

    public int getReasonType() {
        return reasonType;
    }

    public void setReasonType(int reasonType) {
        this.reasonType = reasonType;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
