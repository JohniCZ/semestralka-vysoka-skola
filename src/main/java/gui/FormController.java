package gui;

import database.AttendedCoursesDao;
import database.CoursesDao;
import database.UserDao;
import helpers.AuthorizationService;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import objects.AttendedCourse;
import objects.Course;
import objects.User;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FormController {

    @FXML
    private Label formTitle;

    @FXML
    private VBox formFields;

    @FXML
    private Button closeButton;

    private CoursesDao coursesDao;
    private UserDao userDao;
    private AttendedCoursesDao attendedCoursesDao;
    private Node comboBoxCourses;
    private Node comboBoxCompulsorySubjects;
    private Node comboBoxOptionalSubjects;

    public FormController() {
        coursesDao = new CoursesDao();
        userDao = new UserDao();
        attendedCoursesDao = new AttendedCoursesDao();
    }

    /**
     * Set data for form controller
     *
     * @param title
     * @param formIdent
     */
    public void setData(String title, String formIdent) {
        formTitle.setText("Add new " + title);
        switch (formIdent) {
            case "adminCoursesForm":
                loadAdminCoursesForm();
                break;
            case "adminTeachersForm":
                loadAdminTeachersForm();
                break;
            case "adminStudentsForm":
                loadAdminStudentsForm();
                break;
            case "studentCoursesForm":
                loadStudentCoursesForm();
                break;
        }
    }

    /**
     * Load courses form for admin
     */
    private void loadAdminCoursesForm() {
        TextField textfieldName = createFormTextField("courseName", "Fill course name", 37.0, 473.0);
        formFields.getChildren().add(textfieldName);

        TextArea textAreaDescription = createFormTextArea("courseDescription", "Fill course description");
        formFields.getChildren().add(textAreaDescription);

        TextField textfielCreditValue = createFormTextField("courseCreditValue", "Course Credit Value", 37.0, 473.0);
        formFields.getChildren().add(textfielCreditValue);

        ComboBox comboBoxRecommendedSemester = createFormComboBox("recommendedSemester", "Recommended semester", 37.0, 573.0);
        comboBoxRecommendedSemester.getItems().addAll(
                "1",
                "2",
                "3",
                "4",
                "5",
                "6"
        );
        formFields.getChildren().add(comboBoxRecommendedSemester);
        closeButton.setOnAction(event -> {
            Course course = new Course(textfieldName.getText(), textAreaDescription.getText(), Long.valueOf(textfielCreditValue.getText()),
                    Long.valueOf(comboBoxRecommendedSemester.getSelectionModel().getSelectedItem().toString()), null);

            if (coursesDao.createObject(course) == null) {
                showModal("Fail", "Creating course failed.", "There was some error in creating course.");
            }
            closeForm(event);
        });
    }

    /**
     * Load teachers form for admin
     */
    private void loadAdminTeachersForm() {
        TextField textfieldFirstName = createFormTextField("teacherFirstName", "Fill teacher first name", 37.0, 473.0);
        formFields.getChildren().add(textfieldFirstName);

        TextField textfieldLastName = createFormTextField("teacherLastName", "Fill teacher last name", 37.0, 473.0);
        formFields.getChildren().add(textfieldLastName);

        TextField textfieldEmail = createFormTextField("teacherEmail", "Fill teacher email", 37.0, 473.0);
        formFields.getChildren().add(textfieldEmail);
        PasswordField passwordField = new PasswordField();
        passwordField.setId("teacherPassword");
        passwordField.setPrefHeight(37.0);
        passwordField.setPrefWidth(473.0);

        formFields.getChildren().add(passwordField);
        closeButton.setOnAction(event -> {
            User user = new User();
            user.setEmail(textfieldEmail.getText());
            user.setRole(2L);
            user.setLastname(textfieldLastName.getText());
            user.setFirstname(textfieldFirstName.getText());
            user.setPass(AuthorizationService.encrypt(passwordField.getText()));
            if (userDao.createObject(user) == null) {
                showModal("Fail", "Creating teacher failed.", "There was some error in creating teacher.");
            }
            closeForm(event);
        });
    }

    /**
     * Load students form for admin
     */
    private void loadAdminStudentsForm() {
        TextField textfieldFirstName = createFormTextField("studentFirstName", "Fill student first name", 37.0, 473.0);
        formFields.getChildren().add(textfieldFirstName);

        TextField textfieldLatName = createFormTextField("studentLastName", "Fill student last name", 37.0, 473.0);
        formFields.getChildren().add(textfieldLatName);

        TextField textfieldEmail = createFormTextField("studentEmail", "Fill student email", 37.0, 473.0);
        formFields.getChildren().add(textfieldEmail);

        PasswordField passwordField = new PasswordField();
        passwordField.setId("studentPassword");
        passwordField.setPrefHeight(37.0);
        passwordField.setPrefWidth(473.0);

        formFields.getChildren().add(passwordField);
        closeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                User user = new User();
                user.setEmail(textfieldEmail.getText());
                user.setRole(1L);
                user.setLastname(textfieldLatName.getText());
                user.setFirstname(textfieldFirstName.getText());
                user.setPass(AuthorizationService.encrypt(passwordField.getText()));
                if (userDao.createObject(user) == null) {
                    showModal("Fail", "Creating student failed.", "There was some error in creating student.");
                }
                closeForm(event);
            }
        });
    }

    /**
     * Load courses form for student
     */
    private void loadStudentCoursesForm() {
        ComboBox comboBoxCourses = createFormComboBox("comboBoxCourses", "Select course", 37.0, 573.0);
        JSONArray courses = coursesDao.listAll();
        List<String> coursesList = new ArrayList<>();
        for (Object object : courses) {
            JSONObject jsonObject = (JSONObject) object;
            coursesList.add((String) jsonObject.get("name"));
        }
        comboBoxCourses.getItems().addAll(
                coursesList
        );
        formFields.getChildren().add(comboBoxCourses);
        closeButton.setOnAction(event -> {
            AttendedCourse attendedCourse = new AttendedCourse();
            attendedCourse.setCourseId(coursesDao.getByName(comboBoxCourses.getSelectionModel().getSelectedItem().toString()).getId());
            attendedCourse.setUserId(MainController.getAuthorizationContext().getUser().getId());
            if (attendedCoursesDao.createObject(attendedCourse) == null) {
                showModal("Fail", "Adding course failed.", "There was some error in adding course to student.");
            } else {
                showModal("Success", "Adding course was successful.", "Adding course was successful.");
            }
            closeForm(event);
        });
    }

    /**
     * Create ComboBox for form
     *
     * @param identificator
     * @param prompt
     * @param height
     * @param width
     * @return
     */
    private ComboBox createFormComboBox(String identificator, String prompt, Double height, Double width) {
        ComboBox comboBox = new ComboBox();
        comboBox.setId(identificator);
        comboBox.setPromptText(prompt);
        comboBox.setPrefWidth(width);
        comboBox.setPrefHeight(height);

        return comboBox;
    }

    /**
     * Create TextArea for form
     *
     * @param identificator
     * @param prompt
     * @return
     */
    private TextArea createFormTextArea(String identificator, String prompt) {
        TextArea textArea = new TextArea();
        textArea.setId(identificator);
        textArea.setPromptText(prompt);

        return textArea;
    }

    /**
     * Create TextField for form
     *
     * @param identificator
     * @param prompt
     * @param height
     * @param width
     * @return
     */
    private TextField createFormTextField(String identificator, String prompt, Double height, Double width) {
        TextField textfield = new TextField();
        textfield.setId(identificator);
        textfield.setPromptText(prompt);
        textfield.setPrefHeight(height);
        textfield.setPrefWidth(width);

        return textfield;
    }

    @FXML
    private void closeForm(ActionEvent event) {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }

    /**
     * Method for showing dialogs
     *
     * @param title
     * @param headerText
     * @param contentText
     */
    public static void showModal(String title, String headerText, String contentText) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);

        alert.show();
    }
}
