package gui;

import database.AttendedCoursesDao;
import database.CoursesDao;
import database.UserDao;
import helpers.AuthorizationService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import objects.AuthorizationContext;
import objects.Course;
import objects.User;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainController {
    @FXML
    private BorderPane loginPane;

    @FXML
    private GridPane homePane;

    @FXML
    private GridPane page;

    @FXML
    private Label userName;

    @FXML
    private Label userRole;

    @FXML
    private Label userEmail;

    @FXML
    private Label date;

    @FXML
    private Label pageTitle;

    @FXML
    private VBox userStatictics;

    @FXML
    private HBox homePageMenuBox;

    @FXML
    private Pane pageContent;

    @FXML
    private HBox pageMenuBox;

    @FXML
    private TextField loginField, passwordField;

    private CoursesDao coursesDao;
    private UserDao userDao;
    private AttendedCoursesDao attendedCoursesDao;

    private static AuthorizationContext authorizationContext;

    public static AuthorizationContext getAuthorizationContext() {
        return authorizationContext;
    }

    public MainController() {
        coursesDao = new CoursesDao();
        userDao = new UserDao();
        attendedCoursesDao = new AttendedCoursesDao();
    }

    /**
     * SignUp for user
     *
     * @param event
     */
    @FXML
    private void signUp(ActionEvent event) {
        event.consume();
        authorizationContext = AuthorizationService.authorize(loginField.getText(), passwordField.getText());
        if (authorizationContext != null) {
            loginPane.setVisible(false);
            homePane.setVisible(true);
            loginField.setText("");
            passwordField.setText("");
            System.out.println("AUTHORIZED");
            setUserBasicInformation(authorizationContext.getUser());
        } else {
            //TODO some error
            FormController.showModal("Unathorized", "Unathorized", "Invalid user credentials.");
            System.out.println("UNAUTHORIZED");
        }

    }

    /**
     * LogOut for user
     *
     * @param event
     */
    @FXML
    private void logOut(ActionEvent event) {
        event.consume();
        loginPane.setVisible(true);
        homePane.setVisible(false);
        page.setVisible(false);
        this.authorizationContext = null;
    }

    /**
     * Set user information
     *
     * @param user
     */
    private void setUserBasicInformation(User user) {
        userName.setText(user.getFirstname() + " " + user.getLastname());
        userEmail.setText(user.getEmail());
        setUserRole(user.getRole());

        SimpleDateFormat formatter = new SimpleDateFormat("dd. MM. yyyy");
        Date currentDate = new Date();
        date.setText("Today is " + formatter.format(currentDate));

        setUserStatictics(user.getRole());
        setUserMenu(user.getRole());
    }

    /**
     * Set user role
     *
     * @param roleId
     */
    private void setUserRole(Long roleId) {
        if (roleId == 1L) {
            userRole.setText("Student");
        } else if (roleId == 2L) {
            userRole.setText("Teacher");
        } else if (roleId == 3L) {
            userRole.setText("Admin");
        } else {
            userRole.setText("Role");
        }
    }

    /**
     * Set user statictics
     *
     * @param roleId
     */
    private void setUserStatictics(Long roleId) {
        userStatictics.getChildren().clear();

        if (roleId == 3) {
            userStatictics.getChildren().add(createUserStaticticsLabel("Courses: " + coursesDao.listAll().size(), "home-page-user-information"));
            userStatictics.getChildren().add(createUserStaticticsLabel("Teachers: " + userDao.listAllByRoleId(2L).size(), "home-page-user-information"));
            userStatictics.getChildren().add(createUserStaticticsLabel("Students: " + userDao.listAllByRoleId(3L).size(), "home-page-user-information"));
        } else if (roleId == 2) {
            userStatictics.getChildren().add(createUserStaticticsLabel("Courses: 2", "home-page-user-information"));
            userStatictics.getChildren().add(createUserStaticticsLabel("Students: 35", "home-page-user-information"));
        } else if (roleId == 1) {
            userStatictics.getChildren().add(createUserStaticticsLabel("Courses: " + attendedCoursesDao.getAttendedCoursesByStudent(authorizationContext.getUser().getId()).size(), "home-page-user-information"));
        }
    }

    /**
     * Create label for user statictics
     *
     * @param text
     * @param cssClass
     * @return Label
     */
    private Label createUserStaticticsLabel(String text, String cssClass) {
        Label label = new Label(text);
        label.getStyleClass().add(cssClass);

        return label;
    }

    /**
     * Set items for user menu
     *
     * @param roleId
     */
    private void setUserMenu(Long roleId) {
        homePageMenuBox.getChildren().clear();

        if (roleId == 3) {
            homePageMenuBox.getChildren().add(createMenuButton("Courses", "adminCoursesButton", "menuButton"));
            homePageMenuBox.getChildren().add(createMenuButton("Teachers", "adminTeacherButton", "menuButton"));
            homePageMenuBox.getChildren().add(createMenuButton("Students", "adminStudentButton", "menuButton"));
        } else if (roleId == 2) {
            homePageMenuBox.getChildren().add(createMenuButton("Courses", "teacherCoursesButton", "menuButton"));
            //homePageMenuBox.getChildren().add(createMenuButton("Results", "teacherResultButton", "menuButton"));
        } else if (roleId == 1) {
            homePageMenuBox.getChildren().add(createMenuButton("Schedule", "studentScheduleButton", "menuButton"));
            homePageMenuBox.getChildren().add(createMenuButton("Courses", "studentCoursesButton", "menuButton"));
            //homePageMenuBox.getChildren().add(createMenuButton("Results", "studentResultButton", "menuButton"));
        }
    }

    /**
     * Create UI element button
     *
     * @param text
     * @param identificator
     * @param cssClass
     * @return Button
     */
    private Button createMenuButton(String text, String identificator, String cssClass) {
        Button button = new Button(text);
        button.setId(identificator);
        button.getStyleClass().add(cssClass);
        button.setOnAction((event) -> {
            Button clickedButton = (Button) event.getSource();
            showPage(clickedButton.getId());
        });

        return button;
    }

    /**
     * Show page by button id
     *
     * @param buttonId
     */
    private void showPage(String buttonId) {
        homePane.setVisible(false);
        page.setVisible(true);
        pageMenuBox.getChildren().clear();
        pageContent.getChildren().clear();

        switch (buttonId) {
            case "adminCoursesButton":
                loadAdminCoursesPage();
                break;
            case "adminTeacherButton":
                loadAdminTeachersPage();
                break;
            case "adminStudentButton":
                loadAdminStudentsPage();
                break;
            case "teacherCoursesButton":
                loadTeacherCoursesPage();
                break;
            case "teacherResultButton":
                loadTeacherResultsPage();
                break;
            case "studentScheduleButton":
                loadStudentSchedulePage();
                break;
            case "studentCoursesButton":
                loadStudentCoursesPage();
                break;
            case "studentResultButton":
                loadStudentResultsPage();
                break;
        }
    }

    /**
     * Create course column for table-column
     *
     * @param columnTitle
     * @param columnKey
     * @return
     */
    private TableColumn<String, Course> createCourseColumn(String columnTitle, String columnKey) {
        TableColumn<String, Course> column = new TableColumn<>(columnTitle);
        column.setCellValueFactory(new PropertyValueFactory<>(columnKey));

        return column;
    }

    /**
     * Create course column for table-column
     *
     * @param columnTitle
     * @param columnKey
     * @return
     */
    private TableColumn<String, User> createUserColumn(String columnTitle, String columnKey) {
        TableColumn<String, User> column = new TableColumn<>(columnTitle);
        column.setCellValueFactory(new PropertyValueFactory<>(columnKey));

        return column;
    }


    /**
     * Load course page for admin
     */
    private void loadAdminCoursesPage() {
        pageTitle.setText("Courses");

        TableView tableView = new TableView();
        tableView.setPrefHeight(500);
        tableView.setPrefWidth(1000);

        JSONArray courses = coursesDao.listAll();
        tableView.getColumns().add(createCourseColumn("Name", "name"));
        tableView.getColumns().add(createCourseColumn("Description", "description"));
        tableView.getColumns().add(createCourseColumn("Credits value", "creditsValue"));
        tableView.getColumns().add(createCourseColumn("Recomended semester", "recommendedSemester"));
        tableView.getColumns().add(createCourseColumn("Created at", "dateCreated"));
        for (Object object : courses) {
            JSONObject jsonObject = (JSONObject) object;
            tableView.getItems().add(new Course(jsonObject));
        }

        pageContent.getChildren().add(tableView);

        Button addButton = createPageButton("Add", "addButton", "menuButton");
        addButton.setOnAction((event -> {
            showForm("course", "adminCoursesForm");
        }));
        pageMenuBox.getChildren().add(addButton);

        Button backButton = createPageButton("Back", "backButton", "menuButton");
        backButton.setOnAction((event -> {
            showHomePage();
        }));
        pageMenuBox.getChildren().add(backButton);
    }

    /**
     * Load teachers page for admin
     */
    private void loadAdminTeachersPage() {
        pageTitle.setText("Teachers");

        TableView tableView = new TableView();
        tableView.setPrefHeight(500);
        tableView.setPrefWidth(1000);

        tableView.getColumns().add(createUserColumn("First name", "firstname"));
        tableView.getColumns().add(createUserColumn("Last name", "lastname"));
        tableView.getColumns().add(createUserColumn("Email", "email"));
        tableView.getColumns().add(createUserColumn("Created at", "dateCreated"));
        tableView.getColumns().add(createUserColumn("Last login", "dateLastLogin"));
        JSONArray teachers = userDao.listAllByRoleId(2L);


        for (Object o : teachers) {
            JSONObject jsonObject = (JSONObject) o;
            tableView.getItems().add(new User(jsonObject));
        }
        pageContent.getChildren().add(tableView);
        Button addButton = createPageButton("Add", "addButton", "menuButton");
        addButton.setOnAction((event -> {
            showForm("teacher", "adminTeachersForm");
        }));
        pageMenuBox.getChildren().add(addButton);

        Button backButton = createPageButton("Back", "backButton", "menuButton");
        backButton.setOnAction((event -> {
            showHomePage();
        }));
        pageMenuBox.getChildren().add(backButton);
    }

    /**
     * Load students page for admin
     */
    private void loadAdminStudentsPage() {
        pageTitle.setText("Students");

        TableView tableView = new TableView();
        tableView.setPrefHeight(500);
        tableView.setPrefWidth(1000);

        tableView.getColumns().add(createUserColumn("First name", "firstname"));
        tableView.getColumns().add(createUserColumn("Last name", "lastname"));
        tableView.getColumns().add(createUserColumn("Email", "email"));
        tableView.getColumns().add(createUserColumn("Created at", "dateCreated"));
        tableView.getColumns().add(createUserColumn("Last login", "dateLastLogin"));
        JSONArray students = userDao.listAllByRoleId(1L);

        for (Object o : students) {
            JSONObject jsonObject = (JSONObject) o;
            tableView.getItems().add(new User(jsonObject));
        }


        pageContent.getChildren().add(tableView);

        Button addButton = createPageButton("Add", "addButton", "menuButton");
        addButton.setOnAction((event -> {
            showForm("student", "adminStudentsForm");
        }));
        pageMenuBox.getChildren().add(addButton);

        Button backButton = createPageButton("Back", "backButton", "menuButton");
        backButton.setOnAction((event -> {
            showHomePage();
        }));
        pageMenuBox.getChildren().add(backButton);
    }

    /**
     * Load course page for teacher
     */
    private void loadTeacherCoursesPage() {
        pageTitle.setText("Courses");
        HBox listviewsBox = new HBox();
        listviewsBox.setSpacing(20);

        ListView coursesListView = createContentListView("coursesListView", 500.0, 488.0);

        coursesListView.getItems().add("Programming");
        coursesListView.getItems().add("Economy");
        coursesListView.getItems().add("English");
        coursesListView.getItems().add("Math");

        listviewsBox.getChildren().add(coursesListView);

        // After clicking on the course studentsListView shows students which attend the selected course
        ListView studentsListView = createContentListView("studentsListView", 500.0, 488.0);

        studentsListView.getItems().add("Test Test");
        studentsListView.getItems().add("Teacher Teacher");
        studentsListView.getItems().add("Adam Test");
        studentsListView.getItems().add("Test Test");

        listviewsBox.getChildren().add(studentsListView);

        pageContent.getChildren().add(listviewsBox);

        Button backButton = createPageButton("Back", "backButton", "menuButton");
        backButton.setOnAction((event -> {
            showHomePage();
        }));
        pageMenuBox.getChildren().add(backButton);
    }

    /**
     * Load teachers page for admin
     */
    private void loadTeacherResultsPage() {
        pageTitle.setText("Results");

        ListView resultsListView = createContentListView("resultsListView", 500.0, 1000.0);

        resultsListView.getItems().add("Test Test - Programming - 2");
        resultsListView.getItems().add("Teacher Teacher - Economy - 3");
        resultsListView.getItems().add("Adam Test - Economy - 3");
        resultsListView.getItems().add("Test Test - Math - 2");

        pageContent.getChildren().add(resultsListView);

        Button backButton = createPageButton("Back", "backButton", "menuButton");
        backButton.setOnAction((event -> {
            showHomePage();
        }));
        pageMenuBox.getChildren().add(backButton);
    }

    /**
     * Load course page for teacher
     */
    private void loadStudentSchedulePage() {
        pageTitle.setText("Schedule");

        TableView tableView = new TableView();
        tableView.setPrefHeight(500);
        tableView.setPrefWidth(1000);

        tableView.getColumns().add(createCourseColumn("Name", "name"));
        tableView.getColumns().add(createCourseColumn("Description", "description"));
        tableView.getColumns().add(createCourseColumn("Credits value", "creditsValue"));
        tableView.getColumns().add(createCourseColumn("Recomended semester", "recommendedSemester"));
        tableView.getColumns().add(createCourseColumn("Created at", "dateCreated"));


        pageContent.getChildren().add(tableView);

        Button backButton = createPageButton("Back", "backButton", "menuButton");
        backButton.setOnAction((event -> {
            showHomePage();
        }));
        pageMenuBox.getChildren().add(backButton);
    }

    /**
     * Load course page for teacher
     */
    private void loadStudentCoursesPage() {
        User student = authorizationContext.getUser();
        pageTitle.setText("Courses");

        JSONArray studentCourses = attendedCoursesDao.getAttendedCoursesByStudent(student.getId());
        ListView coursesListView = createContentListView("coursesListView", 500.0, 1000.0);

        List<Long> coursesIds = new ArrayList<>();
        for (Object o : studentCourses) {
            JSONObject jsonObject = (JSONObject) o;
            coursesIds.add((Long) jsonObject.get("courseId"));
        }

        JSONArray courses = coursesDao.getByIds(coursesIds);
        for (Object o : courses) {
            JSONObject jsonObject = (JSONObject) o;
            coursesListView.getItems().add(jsonObject.get("name"));
        }
        pageContent.getChildren().add(coursesListView);

        Button addButton = createPageButton("Add", "addButton", "menuButton");
        addButton.setOnAction((event -> {
            showForm("course", "studentCoursesForm");
        }));
        pageMenuBox.getChildren().add(addButton);

        Button backButton = createPageButton("Back", "backButton", "menuButton");
        backButton.setOnAction((event -> {
            showHomePage();
        }));
        pageMenuBox.getChildren().add(backButton);
    }

    /**
     * Load students page for admin
     */
    private void loadStudentResultsPage() {
        pageTitle.setText("Results");

        ListView resultsListView = createContentListView("resultsListView", 500.0, 1000.0);

        resultsListView.getItems().add("Test Test - Programming - 2");
        resultsListView.getItems().add("Teacher Teacher - Economy - 3");
        resultsListView.getItems().add("Adam Test - Economy - 3");
        resultsListView.getItems().add("Test Test - Math - 2");

        pageContent.getChildren().add(resultsListView);

        Button backButton = createPageButton("Back", "backButton", "menuButton");
        backButton.setOnAction((event -> {
            showHomePage();
        }));
        pageMenuBox.getChildren().add(backButton);
    }

    /**
     * Create ListView for content page
     *
     * @param identificator
     * @param height
     * @param width
     * @return
     */
    private ListView createContentListView(String identificator, Double height, Double width) {
        ListView listView = new ListView();
        listView.setId(identificator);
        listView.setPrefHeight(height);
        listView.setPrefWidth(width);

        return listView;
    }

    /**
     * Show form for creating new items
     */
    private void showForm(String title, String formIdent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/form.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root, 600, 700));
            stage.setTitle(title.substring(0, 1).toUpperCase() + title.substring(1).toLowerCase() + " form");
            FormController controller = fxmlLoader.getController();
            controller.setData(title, formIdent);

            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Show homepage
     */
    private void showHomePage() {
        homePane.setVisible(true);
        page.setVisible(false);
    }

    /**
     * Create page button
     *
     * @param text
     * @param identificator
     * @param cssClass
     * @return
     */
    private Button createPageButton(String text, String identificator, String cssClass) {
        Button button = new Button(text);
        button.setId(identificator);
        button.getStyleClass().add(cssClass);

        return button;
    }

}
