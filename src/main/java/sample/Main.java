package sample;

import database.UserDao;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import gui.MainController;

public class Main extends Application {

    UserDao userDao;

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/main.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);

        MainController controller = loader.getController();

        primaryStage.setTitle("Information system for universities");
        primaryStage.show();
        //TODO použijte, pokud sis chcete vytvořit svojeho usera, tak jen vyplnte udaje sem
/*       userDao = new UserDao();

        User user = new User();
        user.setFirstname("admin");
        user.setLastname("admin");
        user.setPass(AuthorizationService.encrypt("admin"));
        user.setEmail("admin");
        user.setRole(3L);

        JSONObject userObject = userDao.createObject(user);
        user = new User(userObject);
        user.setFirstname("ucitel");
        user.setLastname("ucitel");
        user.setPass(AuthorizationService.encrypt("ucitel"));
        user.setEmail("ucitel");
        user.setRole(2L);

        userObject = userDao.createObject(user);
        user = new User(userObject);*/
    }

    public static void main(String[] args) {
        launch(args);
    }
}
