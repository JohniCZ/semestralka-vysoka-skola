package helpers;

import database.UserDao;
import database.UserRoleDao;
import objects.AuthorizationContext;
import objects.User;
import objects.UserRole;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.Base64;

public final class AuthorizationService {


    private static final String ENCRYPTION_KEY = "oPx57632BwLm8645dBA138UjkslPqmLQ";

    public static String encrypt(String inputText) {
        String strData = "";
        try {
            Key key = new SecretKeySpec(ENCRYPTION_KEY.getBytes(), "AES");

            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            strData = Base64.getEncoder().encodeToString(cipher.doFinal(inputText.getBytes("UTF-8")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strData;
    }

    public static AuthorizationContext authorize(String email, String password) {
        UserRoleDao userRoleDao = new UserRoleDao();
        UserDao userDao = new UserDao();
        password = encrypt(password);
        User user = userDao.loginUserByEmailAndPassword(email, password);
        if (user == null) {
            return null;
        }
        UserRole userRole = new UserRole(userRoleDao.getById(user.getRole()));
        if (userRole == null) {
            return null;
        }
        return new AuthorizationContext(user, userRole);
    }
}
