package database;

import objects.AttendedCourse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class AttendedCoursesDao implements IDatabase {
    private String ATTENDED_COURSES_JSON = "attended_courses.json";

    private static final String KEY_USER_ID = "userId";
    private static final String KEY_COURSE_ID = "courseId";
    private static final String KEY_TYPE = "type";
    private static final String KEY_DATE_CREATED = "dateCreated";

    private static Long currentId = 0L;

    public AttendedCoursesDao() {
    }

    public AttendedCoursesDao(String ATTENDED_COURSES_JSON) {
        this.ATTENDED_COURSES_JSON = ATTENDED_COURSES_JSON;
    }

    @Override
    public JSONObject getById(Long id) {
        JSONArray users = getJsonFile(ATTENDED_COURSES_JSON);
        for (Object object : users) {
            JSONObject jsonObject = (JSONObject) object;

            if (id.equals(jsonObject.get(KEY_ID))) {
                return jsonObject;
            }
        }
        return null;
    }

    @Override
    public JSONObject createObject(Object item) {
        JSONObject userObject = new JSONObject();
        AttendedCourse user = (AttendedCourse) item;
        userObject.put(KEY_COURSE_ID, user.getCourseId());
        userObject.put(KEY_TYPE, user.getType());
        userObject.put(KEY_USER_ID, user.getUserId());
        userObject.put(KEY_DATE_CREATED, user.getDateCreated());

        JSONArray users = this.getJsonFile(ATTENDED_COURSES_JSON);
        currentId = (long) users.size() + 1;
        user.setId(currentId);
        userObject.put(KEY_ID, currentId);
        users.add(userObject);

        //Write JSON file

        try (Writer out = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(ATTENDED_COURSES_JSON), StandardCharsets.UTF_8))) {
            out.write(users.toJSONString());
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userObject;
    }

    @Override
    public JSONArray listAll() {
        return getJsonFile(ATTENDED_COURSES_JSON);
    }

    @Override
    public boolean deleteById() {
        return false;
    }

    public JSONArray getAttendedCoursesByStudent(Long studentId) {
        JSONArray attendedCourses = getJsonFile(ATTENDED_COURSES_JSON);
        JSONArray courses = new JSONArray();
        for (Object object : attendedCourses) {
            JSONObject jsonObject = (JSONObject) object;
            if (studentId.equals(jsonObject.get(KEY_USER_ID))) {
                courses.add(jsonObject);
            }
        }
        return courses;
    }
}
