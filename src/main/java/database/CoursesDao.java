package database;

import objects.Course;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class CoursesDao implements IDatabase {
    private static Long currentId = 0L;
    private String COURSES_JSON_NAME = "courses.json";

    private static final String KEY_NAME = "name";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_CREDITS_VALUE = "creditsValue";
    private static final String KEY_RECOMMENDED_SEMESTER = "recommendedSemester";
    private static final String KEY_DATE_CREATED = "dateCreated";

    public CoursesDao() {
    }

    public CoursesDao(String COURSES_JSON_NAME) {
        this.COURSES_JSON_NAME = COURSES_JSON_NAME;
    }

    @Override
    public JSONObject getById(Long id) {
        JSONArray users = getJsonFile(COURSES_JSON_NAME);
        for (Object object : users) {
            JSONObject jsonObject = (JSONObject) object;
            if (id.equals(jsonObject.get(this.KEY_ID))) {
                return jsonObject;
            }
        }
        return null;
    }

    @Override
    public JSONObject createObject(Object item) {
        JSONObject courseObject = new JSONObject();
        Course course = (Course) item;
        courseObject.put(KEY_NAME, course.getName() );
        courseObject.put(KEY_DESCRIPTION, course.getDescription());
        courseObject.put(KEY_CREDITS_VALUE, course.getCreditsValue());
        courseObject.put(KEY_RECOMMENDED_SEMESTER, course.getRecommendedSemester());
        courseObject.put(KEY_DATE_CREATED, course.getDateCreated());

        JSONArray users = this.getJsonFile(COURSES_JSON_NAME);
        this.currentId = (long) (users.size() + 1);
        course.setId(this.currentId);
        courseObject.put(KEY_ID, this.currentId);
        users.add(courseObject);

        //Write JSON file
        try (FileWriter file = new FileWriter(COURSES_JSON_NAME)) {
            file.write(users.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return courseObject;
    }

    @Override
    public JSONArray listAll() {
        return getJsonFile(COURSES_JSON_NAME);
    }

    @Override
    public boolean deleteById() {
        return false;
    }

    public Course getByName(String name) {
        JSONArray courses = getJsonFile(COURSES_JSON_NAME);
        for (Object object : courses) {
            JSONObject jsonObject = (JSONObject) object;
            if (name.equals(jsonObject.get(KEY_NAME)) || StringUtils.difference(name, (String) jsonObject.get(KEY_NAME)).length() < 2) {
                return new Course(jsonObject);
            }
        }
        return null;
    }

    public JSONArray getByIds(List<Long> ids) {
        JSONArray users = getJsonFile(COURSES_JSON_NAME);
        JSONArray returnArray = new JSONArray();
        for (Object object : users) {
            JSONObject jsonObject = (JSONObject) object;
            if (ids.contains(jsonObject.get(KEY_ID))) {
                returnArray.add(jsonObject);
            }
        }
        return returnArray;
    }
}
