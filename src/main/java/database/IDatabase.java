package database;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public interface IDatabase {
    String KEY_ID = "id";

    public JSONObject getById(Long id);

    public JSONObject createObject(Object item);

    default public JSONArray getJsonFile(String jsonName) {
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader(jsonName)) {
            Object obj = jsonParser.parse(reader);
            return (JSONArray) obj;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new JSONArray();
    }

    public JSONArray listAll();

    public boolean deleteById();
}
