package database;

import objects.UserRole;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class UserRoleDao implements IDatabase {
    private String USER_ROLE_JSON = "user_role.json";

    private static final String KEY_DATE_CREATED = "dateCreated";
    private static final String KEY_DATE_NAME = "name";
    private static final String KEY_DATE_DESCRIPTION = "description";

    private static Long currentId = 0L;

    public UserRoleDao() {
    }

    public UserRoleDao(String USER_ROLE_JSON) {
        this.USER_ROLE_JSON = USER_ROLE_JSON;
    }

    @Override
    public JSONObject getById(Long id) {
        JSONArray users = getJsonFile(USER_ROLE_JSON);
        for (Object object : users) {
            JSONObject jsonObject = (JSONObject) object;

            if (id.equals(jsonObject.get(KEY_ID))) {
                return jsonObject;
            }
        }
        return null;
    }

    @Override
    public JSONObject createObject(Object item) {
        JSONObject courseObject = new JSONObject();
        UserRole course = (UserRole) item;
        courseObject.put(KEY_DATE_NAME, course.getName());
        courseObject.put(KEY_DATE_DESCRIPTION, course.getDescription());
        courseObject.put(KEY_DATE_CREATED, course.getDateCreated());

        JSONArray users = this.getJsonFile(USER_ROLE_JSON);
        this.currentId = (long) users.size() + 1;
        course.setId(this.currentId);
        courseObject.put(KEY_ID, this.currentId);
        users.add(courseObject);

        //Write JSON file
        try (FileWriter file = new FileWriter(USER_ROLE_JSON)) {
            file.write(users.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return courseObject;
    }

    @Override
    public JSONArray listAll() {
        return getJsonFile(USER_ROLE_JSON);
    }

    @Override
    public boolean deleteById() {
        return false;
    }

}
