package database;

import objects.User;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class UserDao implements IDatabase {
    private String PERSON_JSON_NAME = "persons.json";
    private static final String KEY_FIRST_NAME = "firstname";
    private static final String KEY_LAST_NAME = "lastname";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_PASSWORD = "pass";
    private static final String KEY_DATE_CREATED = "dateCreated";
    private static final String KEY_DATE_LAST_LOGIN = "dateLastLogin";
    private static final String KEY_DATE_LAST_ACTION = "dateLastAction";
    private static final String KEY_IS_ACTIVE = "isActive";
    private static final String KEY_ROLE = "role";

    private static Long currentId = 0L;

    public UserDao() {
    }

    public UserDao(String PERSON_JSON_NAME) {
        this.PERSON_JSON_NAME = PERSON_JSON_NAME;
    }

    @Override
    public JSONObject getById(Long id) {
        JSONArray users = getJsonFile(PERSON_JSON_NAME);
        for (Object object : users) {
            JSONObject jsonObject = (JSONObject) object;
            if (id.equals(jsonObject.get(this.KEY_ID))) {
                return jsonObject;
            }
        }
        return null;
    }

    @Override
    public JSONObject createObject(Object item) {
        JSONObject userObject = new JSONObject();
        User user = (User) item;
        userObject.put(KEY_FIRST_NAME, user.getFirstname());
        userObject.put(KEY_LAST_NAME, user.getLastname());
        userObject.put(KEY_EMAIL, user.getEmail());
        userObject.put(KEY_PASSWORD, user.getPass());
        userObject.put(KEY_DATE_CREATED, user.getDateCreated());
        userObject.put(KEY_DATE_LAST_LOGIN, user.getDateLastLogin());
        userObject.put(KEY_DATE_LAST_ACTION, user.getDateLastAction());
        userObject.put(KEY_IS_ACTIVE, user.isActive());
        userObject.put(KEY_ROLE, user.getRole());

        JSONArray users = this.getJsonFile(PERSON_JSON_NAME);
        currentId = (long) users.size() + 1;
        user.setId(currentId);
        userObject.put(KEY_ID, currentId);
        users.add(userObject);

        //Write JSON file

        try (Writer out = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(PERSON_JSON_NAME), StandardCharsets.UTF_8))) {
            out.write(users.toJSONString());
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userObject;
    }

    @Override
    public JSONArray listAll() {
        return getJsonFile(PERSON_JSON_NAME);
    }

    @Override
    public boolean deleteById() {
        return false;
    }

    public User loginUserByEmailAndPassword(String email, String password) {
        JSONArray users = getJsonFile(PERSON_JSON_NAME);
        for (Object object : users) {
            JSONObject jsonObject = (JSONObject) object;
            if (email.equals(jsonObject.get(KEY_EMAIL)) && password.equals(jsonObject.get(KEY_PASSWORD))) {
                return new User(jsonObject);
            }
        }
        return null;
    }

    public JSONArray listAllByRoleId(Long roleId) {
        JSONArray users = getJsonFile(PERSON_JSON_NAME);
        JSONArray toReturn = new JSONArray();
        for (Object object : users) {
            JSONObject jsonObject = (JSONObject) object;
            if (roleId.equals(jsonObject.get(KEY_ROLE))) {
                toReturn.add(jsonObject);
            }
        }
        return toReturn;
    }
}
