package database;

import objects.CreditOption;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class CreditDao implements IDatabase {
    private static Long currentId = 0L;
    private String CREDIT_JSON_NAME = "credit.json";

    private static final String KEY_USER_ID = "userId";
    private static final String KEY_CREDITS_VALUE = "creditsValue";
    private static final String KEY_REASON_TYPE = "reasonType";
    private static final String KEY_STATE = "state";
    private static final String KEY_DATE_CREATED = "dateCreated";

    public CreditDao() {
    }

    public CreditDao(String CREDIT_JSON_NAME) {
        this.CREDIT_JSON_NAME = CREDIT_JSON_NAME;
    }

    @Override
    public JSONObject getById(Long id) {
        JSONArray users = getJsonFile(CREDIT_JSON_NAME);
        for (Object object : users) {
            JSONObject jsonObject = (JSONObject) object;
            if (id.equals(jsonObject.get(this.KEY_ID))) {
                return jsonObject;
            }
        }
        return null;
    }

    @Override
    public JSONObject createObject(Object item) {
        CreditOption creditOption = (CreditOption) item;
        JSONObject creditObject = new JSONObject();
        creditObject.put(KEY_CREDITS_VALUE, creditOption.getCreditsValue());
        creditObject.put(KEY_REASON_TYPE, creditOption.getReasonType());
        creditObject.put(KEY_STATE, creditOption.getState());
        creditObject.put(KEY_USER_ID, creditOption.getUserId());
        creditObject.put(KEY_DATE_CREATED, creditOption.getDateCreated());

        JSONArray credits = this.getJsonFile(CREDIT_JSON_NAME);
        this.currentId = (long) credits.size() + 1;
        creditOption.setId(this.currentId);
        creditObject.put(KEY_ID, this.currentId);
        credits.add(creditObject);

        //Write JSON file
        try (FileWriter file = new FileWriter(CREDIT_JSON_NAME)) {
            file.write(credits.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return creditObject;
    }

    @Override
    public JSONArray listAll() {
        return getJsonFile(CREDIT_JSON_NAME);
    }

    @Override
    public boolean deleteById() {
        return false;
    }

}
