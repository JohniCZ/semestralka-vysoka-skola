# 2. semestrální úloha – Vysoká škola
Cílem této semestrální je navrhnout a následně implementovat informační systém pro vysokou školu. V systému bude umožněno patřičné osobě spravovat učitele, studenty, studijní programy a předměty. U předmětů bude systém zpracovávat informace o termínech, kreditech a přiřazený učitel. Učitel bude moct zadávat k jeho předmětů průběžné a závěrečné výsledky žákům. Žák si může zapisovat předměty a zkoušky, dále bude moci si zobrazit svoje průběžné a závěrečné výsledky.

## Členové týmu
*  Martin Zbořil – Use case diagram
*  Jan Placek – Návrh UI
*  Matěj Štágl – Class diagram
*  Jan Švejda – Struktura databáze, testovací případy
*  Jan Andrášek
 
## Use case diagram
Use case diagram, pro náš systém určený vysokým školy, obsahuje čtyři aktéry:
*  Student
*  Učitel
*  Admin
*  Uživatel

V našem diagramu případu užití je také obsažena generalizace. Konkrétně je tento vztah mezi **uživatelem**, který zobecňuje aktéry student, učitel a admin. Pomocí tohoto vztahu je pak znázorněn případ užití *Přihlásit se do systému*, jenž je určen pro všechny aktéry.
Use case diagram obsahuje následující případy užití:

![UseCase](/uploads/9e5a51c380e5b2741994c5579a701d89/UseCase.png)

### Popis jednotlivých případů užití

| Název případu užití | Vyhledat předmět |
| ------ | ------ |
| Identifikace | UC1 |
| Cíl | Vyhledat vybraný předmět | 
| Aktéři | Student |
| Vstupní podmínky | Student se úspěšně přihlásil a zvolil možnost vyhledat předmět |
| Scénáře | Systém vybere seznam dostupných předmětů <br> Pokud existuje alespoň jeden předmět, systém je předá studentovi <br> Systém zobrazí studentovi formulář, ve kterém mu bude umožněno vyfiltrovat si potřebný předmět/y <br> Aktér zadá do formulář patřičné parametry, např. název předmětu, ident nebo pro jaký je předmět určen semestr... <br>Aktér zvolí tlačítko pro odeslání formuláře <br> Systém ověří zaslané údaje a vybere předměty <br> Systém zobrazí studentovi předměty, dle jeho požadavků |
| Alternativní scénář – žádný předmět | Systém nenalezne žádný předmět <br> Systém vypíše hlášku studentovi |
| Alternativní scénář – ověření formuláře | Systém vyhodnotí zadané údaje jako chybné <br> Systém vrátí chybovou hlášku studentovi |
| Výstupní podmínky | Student úspěšně našel předmět |

| Název případu užití | Zapsat předmět |
| ------ | ------ |
| Identifikace | UC2 |
| Cíl | Zapsání vybraného předmětu | 
| Aktéři | Student |
| Vstupní podmínky | Student je přihlášen do systému a vyhledal si předmět, který si chce zapsat |
| Scénáře | Student zvolí tlačítko zapsat předmět <br> Systém převezme parametry a zkontroluje zda-li předmět existuje <br> Pokud všechny data jsou v pořádku, předmět zapíše |
| Alternativní scénář – předmět neexistuje | Systém ověří zaslaný požadavek <br> Systém nenalezne daný předmět <br> Systém vypíše chybovou hlášku studentovi |
| Výstupní podmínky | Student si úspěšně zapsal předmět |

| Název případu užití | Zrušit zapsání předmětu |
| ------ | ------ |
| Identifikace | UC3 |
| Cíl | Zrušení zapsaného předmětu | 
| Aktéři | Student |
| Vstupní podmínky | Student je přihlášen do systému a má zapsaný předmět |
| Scénáře | Student vyhledá zapsaný předmět, který chce zrušit, pomocí formuláře <br> Student odešle data přes formulář pro vyhledání předmětu <br> Systém zkontroluje data a vrátí studentovi vybraný předmět <br> Studentovi se zobrazí vybraný předmět s tlačítkem zrušit <br> Student zvolí tlačítko zrušit <br> Systém zruší zápis předmětu studentovi |
| Alternativní scénář – předmět neexistuje | Systém ověří zaslaný požadavek <br> Systém nenalezne daný předmět <br> Systém vypíše chybovou hlášku studentovi |
| Výstupní podmínky | Student si úspěšně zrušil zápis vybraného předmětu |

| Název případu užití | Zobrazit rozvrh |
| ------ | ------ |
| Identifikace | UC4 |
| Cíl | Zobrazit rozvrh ze zapsaných předmětů | 
| Aktéři | Student |
| Vstupní podmínky | Student je přihlášen do systému |
| Scénáře | Student zvolí možnost zobrazit rozvrh <br> Systém vybere studentovi zapsané předměty <br> Systém sestaví studentovi rozvrh <br> Sestavený rozvrh vrátí studentovi |
| Alternativní scénář – žádné předměty | Systém nenalezne žádné předměty <br> Systém informuje žáka, že nemá zapsané žádné předměty |
| Výstupní podmínky | Studentovi se zobrazí rozvrh jeho zapsaných předmětů |

| Název případu užití | Zobrazit průběžné výsledky |
| ------ | ------ |
| Identifikace | UC5 |
| Cíl | Zobrazení průběžných výsledků | 
| Aktéři | Student |
| Vstupní podmínky | Student je přihlášen do systému |
| Scénáře | Student zvolí možnost zobrazit průběžné výsledky <br> Systém sestaví studentovi průběžné výsledky <br> Systém vrátí studentovi průběžné výsledky |
| Alternativní scénář – žádné průběžné výsledky | Systém nenalezne žádné průběžné výsledky <br> Systém informuje žáka, že nemá žádné průběžné výsledky |
| Výstupní podmínky | Studentovi se zobrazí průběžné výsledky |

| Název případu užití | Zapsat zkoušku |
| ------ | ------ |
| Identifikace | UC6 |
| Cíl | Zapsání vybrané zkoušky | 
| Aktéři | Student |
| Vstupní podmínky | Student je přihlášen do systému a vyhledal si předmět, u kterého si chce zapsat zkoušku |
| Scénáře | Student zvolí tlačítko zapsat zkoušku <br> Systém převezme parametry a ověří data <br> Pokud všechny data jsou v pořádku, zkoušku zapíše |
| Alternativní scénář – zkouška neexistuje | Systém při ověření dat nenalezne zkoušku <br> Systém informuje žáka, že zkoušku nenalezl |
| Výstupní podmínky | Student si úspěšně zapsal zkoušku |

| Název případu užití | Zrušit zapsání zkoušky |
| ------ | ------ |
| Identifikace | UC7 |
| Cíl | Zrušení zapsané zkoušky | 
| Aktéři | Student |
| Vstupní podmínky | Student je přihlášen do systému a má zapsanou zkoušku |
| Scénáře | Student vyhledá zapsanou zkoušku, kterou chce zrušit <br> Student odešle data přes formulář pro vyhledání předmětu <br> Systém ověří, zda-li zkouška existuje <br> Systém zruší zápis zkoušky studentovi |
| Alternativní scénář – zkouška neexistuje | Systém při ověření dat nenalezne zkoušku <br> Systém informuje žáka, že zkoušku nenalezl |
| Výstupní podmínky | Student si úspěšně zrušil zápis vybraného předmětu |

| Název případu užití | Zobrazit závěrečné výsledky |
| ------ | ------ |
| Identifikace | UC8 |
| Cíl | Zobrazení závěrečných výsledků | 
| Aktéři | Student |
| Vstupní podmínky | Student je přihlášen do systému |
| Scénáře | Student zvolí možnost zobrazit závěrečné výsledky <br> Systém sestaví studentovi závěrečné výsledky <br> Systém vrátí studentovi průběžné výsledky |
| Alternativní scénář – žádné průběžné výsledky | Systém nenalezne žádné závěrečné výsledky <br> Systém informuje žáka, že nemá žádné závěrečné výsledky |
| Výstupní podmínky | Studentovi se zobrazí závěrečné výsledky |

| Název případu užití | Přihlásit se do systému |
| ------ | ------ |
| Identifikace | UC9 |
| Cíl | Přihlášení do systému | 
| Aktéři | Uživatel |
| Vstupní podmínky | - |
| Scénáře | Uživatel zadá svoje údaje <br> Systém tyto údaje ověří <br> Pokud ověření proběhne v pořádku, je uživatel přihlášen  |
| Alternativní scénář – chybné ověření | Systém při ověření dat, vyhodnotí chybu <br> Systém vrátí uživateli chybovou hlášku |
| Výstupní podmínky | Uživatel je přihlášen |

| Název případu užití | Zobrazit přiřazené předměty |
| ------ | ------ |
| Identifikace | UC10 |
| Cíl | Zobrazení přiřazených předmětů | 
| Aktéři | Učitel |
| Vstupní podmínky | Učitel je přihlášen do systému |
| Scénáře | Učitel zvolí zobrazit přiřazené předměty <br> Systém sestaví seznam předmětu, jenž jsou přiřazené učiteli <br> Systém vrátí učiteli jeho seznam předmětů |
| Alternativní scénář – žádné předměty | Systém nenalezne žádné předměty <br> Systém vrátí učiteli informaci, že nemá žádné předměty |
| Výstupní podmínky | Učitel obdrží jeho seznam předmětů |

| Název případu užití | Zobrazit žáky |
| ------ | ------ |
| Identifikace | UC11 |
| Cíl | Zobrazit žáky zapsané na předměty | 
| Aktéři | Učitel |
| Vstupní podmínky | Učitel je přihlášen do systému a zobrazil si přiřazené předměty |
| Scénáře | Učitel u předmětu možnost zobrazit žáky <br> Systém zobrazí žáky zapsané na předmět, který učitel vybral <br> Systém vrátí učiteli seznam jeho žáků |
| Alternativní scénář – žádní žáci | Systém nenalezne žádné žáky u předmětu <br> Systém vrátí učiteli informaci, že nemá žádné žáky |
| Výstupní podmínky | Učiteli se zobrazí jeho žáci zapsaný na vybraný předmět |

| Název případu užití | Zadat průběžné výsledky |
| ------ | ------ |
| Identifikace | UC12 |
| Cíl | Zadat žákovi průběžné výsledky | 
| Aktéři | Učitel |
| Vstupní podmínky | Učitel je přihlášen do systému, zobrazil si přiřazené předměty a zvolí zobrazení žáků zapsaných na jeho předmět |
| Scénáře | Učitel u vybraného žáka zvolí možnost zadat průběžné výsledky <br> Systém ověří data <br> Systém vrátí učiteli formulář <br> Učitel zadá do formuláře data <br> Systém zkontroluje zaslaná data <br> Systém uloží průběžný výsledek předmětu k vybranému žákovi |
| Alternativní scénář – validace formuláře | Učitel odeslal data <br> Systém našel chybu při validaci dat <br> Systém vrátí učiteli chybovou hlášku |
| Výstupní podmínky | Učitel zadal studentovi průběžný výsledek |

| Název případu užití | Vypsat termín zkoušky |
| ------ | ------ |
| Identifikace | UC13 |
| Cíl | Učitel vypíše termín zkoušky pro předmět | 
| Aktéři | Učitel |
| Vstupní podmínky | Učitel je přihlášen do systému a má vybraný předmět |
| Scénáře | Učitel zvolí možnost vypsat termín zkoušky u vybraného předmětu <br> Systém zobrazí učiteli formulář <br> Učitel zadá informace o termínu zkoušky <br> Učitel odešle formulář <br> Systém zkontroluje zaslané údaje <br> Systém uloží termín zkoušky |
| Alternativní scénář – validace formuláře | Učitel odeslal data <br> Systém našel chybu při validaci dat <br> Systém vrátí učiteli chybovou hlášku |
| Výstupní podmínky | Učitel úspěšně vypsal termín zkoušky |

| Název případu užití | Odebrat termín zkoušky |
| ------ | ------ |
| Identifikace | UC14 |
| Cíl | Učitel úspěšně odebere termín zkoušky | 
| Aktéři | Učitel |
| Vstupní podmínky | Učitel je přihlášen do systému a má vybraný předmět u kterého vypsal termín zkoušky |
| Scénáře | Učitel zvolí možnost odebrat termín zkoušky <br> Systém zkontroluje učitelův požadavek <br> Systém odebere termín zkoušky |
| Alternativní scénář – chybný požadavek | Učitel odeslal požadavek <br> Systém při kontrole vyhodnotil chybu <br> Systém vrátil chybovou hlášku učiteli |
| Výstupní podmínky | Učitel úspěšně odebral termín zkoušky |

| Název případu užití | Změnit termín zkoušky |
| ------ | ------ |
| Identifikace | UC15 |
| Cíl | Učitel úspěšně změní termín zkoušky | 
| Aktéři | Učitel |
| Vstupní podmínky | Učitel je přihlášen do systému a má vybraný předmět u kterého vypsal termín zkoušky |
| Scénáře | Učitel zvolí možnost změnit termín zkoušky <br> Systém zkontroluje učitelův požadavek <br> Systém změní termín zkoušky |
| Alternativní scénář – chybný požadavek | Učitel odeslal požadavek <br> Systém při kontrole vyhodnotil chybu <br> Systém vrátil chybovou hlášku učiteli |
| Výstupní podmínky | Učitel úspěšně změnil termín zkoušky |

| Název případu užití | Zadat závěrečné výsledky |
| ------ | ------ |
| Identifikace | UC16 |
| Cíl | Učitel úspěšně zadá závěrečné výsledky | 
| Aktéři | Učitel |
| Vstupní podmínky | Učitel je přihlášen do systému a má vybraný předmět |
| Scénáře | Učitel u vybraného žáka zvolí možnost zadat závěrečný výsledek <br> Systém ověří data <br> Systém vrátí učiteli formulář <br> Učitel zadá do formuláře data <br> Systém zkontroluje zaslaná data <br> Systém uloží závěrečný výsledek předmětu k vybranému žákovi |
| Alternativní scénář – validace formulář | Učitel odeslal data <br> Systém našel chybu při validaci dat <br> Systém vrátí učiteli chybovou hlášku |
| Výstupní podmínky | Učitel zadal závěrečný výsledek studentovi |

| Název případu užití | Vytvořit učitele |
| ------ | ------ |
| Identifikace | UC17 |
| Cíl | Vytvořit učitele | 
| Aktéři | Admin |
| Vstupní podmínky | Admin je přihlášen do systému |
| Scénáře | Admin zvolí možnost vytvořit učitele <br> Systém zobrazí adminovi formulář <br> Admin formulář vyplní a odešle <br> Systém formulář ověří <br> Systém uloží data |
| Alternativní scénář – validace formulář | Admin odeslal data <br> Systém našel chybu při validaci dat <br> Systém vrátí admin chybovou hlášku |
| Výstupní podmínky | Admin vytvoří učitel |

| Název případu užití | Změnit údaje učitele |
| ------ | ------ |
| Identifikace | UC18 |
| Cíl | Změnit učitelovi údaje | 
| Aktéři | Admin |
| Vstupní podmínky | Admin je přihlášen do systému a vytvořil učitele |
| Scénáře | Admin zvolí u učitele možnost změnit <br> Systém zobrazí adminovi formulář <br> Admin formulář vyplní a odešle <br> Systém formulář ověří <br> Systém uloží data |
| Alternativní scénář – validace formulář | Admin odeslal data <br> Systém našel chybu při validaci dat <br> Systém vrátí admin chybovou hlášku |
| Výstupní podmínky | Admin změní učitelovi údaje |

| Název případu užití | Smazat učitele |
| ------ | ------ |
| Identifikace | UC19 |
| Cíl | Smazat učitele | 
| Aktéři | Admin |
| Vstupní podmínky | Admin je přihlášen do systému a vytvořil učitele |
| Scénáře | Admin zvolí u učitele možnost smazat <br> Systém formulář ověří <br> Systém smaže učitele |
| Alternativní scénář – chybné ověření požadavku | Admin odeslal požadavek <br> Systém našel chybu při ověření chybu <br> Systém vrátí admin chybovou hlášku |
| Výstupní podmínky | Admin smaže učitele |

| Název případu užití | Vytvořit žáka |
| ------ | ------ |
| Identifikace | UC20 |
| Cíl | Vytvořit žáka | 
| Aktéři | Admin |
| Vstupní podmínky | Admin je přihlášen do systému |
| Scénáře | Admin zvolí možnost vytvořit žáka <br> Systém zobrazí adminovi formulář <br> Admin formulář vyplní a odešle <br> Systém formulář ověří <br> Systém uloží data |
| Alternativní scénář – validace formulář | Admin odeslal data <br> Systém našel chybu při validaci dat <br> Systém vrátí admin chybovou hlášku |
| Výstupní podmínky | Admin vytvoří žáka |

| Název případu užití | Změnit údaje žáka |
| ------ | ------ |
| Identifikace | UC21 |
| Cíl | Změnit žákovi údaje | 
| Aktéři | Admin |
| Vstupní podmínky | Admin je přihlášen do systému a vytvořil žáka |
| Scénáře | Admin zvolí u žáka možnost změnit <br> Systém zobrazí adminovi formulář <br> Admin formulář vyplní a odešle <br> Systém formulář ověří <br> Systém uloží data |
| Alternativní scénář – validace formulář | Admin odeslal data <br> Systém našel chybu při validaci dat <br> Systém vrátí admin chybovou hlášku |
| Výstupní podmínky | Admin změní žákovi údaje |

| Název případu užití | Smazat žáka |
| ------ | ------ |
| Identifikace | UC22 |
| Cíl | Smazat žáka | 
| Aktéři | Admin |
| Vstupní podmínky | Admin je přihlášen do systému a vytvořil žáka |
| Scénáře | Admin zvolí u žáka možnost smazat <br> Systém formulář ověří <br> Systém smaže žáka |
| Alternativní scénář – chybné ověření požadavku | Admin odeslal požadavek <br> Systém našel chybu při ověření chybu <br> Systém vrátí admin chybovou hlášku |
| Výstupní podmínky | Admin smaže žáka |

| Název případu užití | Vytvořit studijní program |
| ------ | ------ |
| Identifikace | UC23 |
| Cíl | Vytvořit studijní program | 
| Aktéři | Admin |
| Vstupní podmínky | Admin je přihlášen do systému |
| Scénáře | Admin zvolí možnost vytvořit studijní program <br> Systém zobrazí adminovi formulář <br> Admin formulář vyplní a odešle <br> Systém formulář ověří <br> Systém uloží data |
| Alternativní scénář – validace formulář | Admin odeslal data <br> Systém našel chybu při validaci dat <br> Systém vrátí admin chybovou hlášku |
| Výstupní podmínky | Admin vytvoří studijní program |

| Název případu užití | Změnit údaje o studijním programu |
| ------ | ------ |
| Identifikace | UC24 |
| Cíl | Změnit údaje o studijním programu | 
| Aktéři | Admin |
| Vstupní podmínky | Admin je přihlášen do systému a vytvořil studijní program |
| Scénáře | Admin zvolí u studijního programu možnost změnit <br> Systém zobrazí adminovi formulář <br> Admin formulář vyplní a odešle <br> Systém formulář ověří <br> Systém uloží data |
| Alternativní scénář – validace formulář | Admin odeslal data <br> Systém našel chybu při validaci dat <br> Systém vrátí admin chybovou hlášku |
| Výstupní podmínky | Admin změní údaje o studijním programu |

| Název případu užití | Smazat studijní program |
| ------ | ------ |
| Identifikace | UC25 |
| Cíl | Smazat studijní program | 
| Aktéři | Admin |
| Vstupní podmínky | Admin je přihlášen do systému a vytvořil studijní program |
| Alternativní scénář – chybné ověření požadavku | Admin odeslal požadavek <br> Systém našel chybu při ověření chybu <br> Systém vrátí admin chybovou hlášku |
| Scénáře | Admin zvolí u studijního programu možnost smazat <br> Systém smaže studijní program |
| Výstupní podmínky | Admin smaže studijní program |

| Název případu užití | Přidat předmět |
| ------ | ------ |
| Identifikace | UC26 |
| Cíl | Přidat předmět | 
| Aktéři | Admin |
| Vstupní podmínky | Admin je přihlášen do systému a vytvořil studijní program |
| Scénáře | Admin zvolí možnost vytvořit předmět <br> Systém zobrazí adminovi formulář <br> Admin formulář vyplní, přiřadí předmět k studijnímu programu a odešle <br> Systém formulář ověří <br> Systém uloží data |
| Alternativní scénář – validace formulář | Admin odeslal data <br> Systém našel chybu při validaci dat <br> Systém vrátí admin chybovou hlášku |
| Výstupní podmínky | Admin vytvoří předmět |

| Název případu užití | Změnit údaje o předmětu |
| ------ | ------ |
| Identifikace | UC27 |
| Cíl | Změnit údaje o předmětu | 
| Aktéři | Admin |
| Vstupní podmínky | Admin je přihlášen do systému, vytvořil studijní program a předmět |
| Scénáře | Admin zvolí u předmětu možnost změnit <br> Systém zobrazí adminovi formulář <br> Admin formulář vyplní a odešle <br> Systém formulář ověří <br> Systém uloží data |
| Alternativní scénář – validace formulář | Admin odeslal data <br> Systém našel chybu při validaci dat <br> Systém vrátí admin chybovou hlášku |
| Výstupní podmínky | Admin změní údaje o předmětu |

| Název případu užití | Smazat předmět |
| ------ | ------ |
| Identifikace | UC28 |
| Cíl | Smazat údaje o předmětu | 
| Aktéři | Admin |
| Vstupní podmínky | Admin je přihlášen do systému, vytvořil studijní program a předmět |
| Scénáře | Admin zvolí u předmětu možnost smazat <br> Systém smaže data |
| Alternativní scénář – chybné ověření požadavku | Admin odeslal požadavek <br> Systém našel chybu při ověření chybu <br> Systém vrátí admin chybovou hlášku |
| Výstupní podmínky | Admin smaže předmět |

| Název případu užití | Přidat termín k předmětu |
| ------ | ------ |
| Identifikace | UC29 |
| Cíl | Přidat termín k předmětu | 
| Aktéři | Admin |
| Vstupní podmínky | Admin je přihlášen do systému, vytvořil studijní program a předmět |
| Scénáře | Admin zvolí u předmětu možnost přidat termín <br> Systém zobrazí adminovi formulář <br> Admin formulář vyplní a odešle <br> Systém formulář ověří <br> Systém uloží data |
| Alternativní scénář – validace formulář | Admin odeslal data <br> Systém našel chybu při validaci dat <br> Systém vrátí admin chybovou hlášku |
| Výstupní podmínky | Admin vytvoří přidá termín k předmětu |

| Název případu užití | Změnit údaje termínu |
| ------ | ------ |
| Identifikace | UC30 |
| Cíl | Změnit údaje termínu | 
| Aktéři | Admin |
| Vstupní podmínky | Admin je přihlášen do systému, vytvořil studijní program, předmět a termín |
| Scénáře | Admin zvolí u předmětu možnost změnit termín <br> Systém zobrazí adminovi formulář <br> Admin formulář vyplní a odešle <br> Systém formulář ověří <br> Systém uloží data |
| Alternativní scénář – validace formulář | Admin odeslal data <br> Systém našel chybu při validaci dat <br> Systém vrátí admin chybovou hlášku |
| Výstupní podmínky | Admin změní termín |

| Název případu užití | Smazat termín |
| ------ | ------ |
| Identifikace | UC31 |
| Cíl | Smazat termín | 
| Aktéři | Admin |
| Vstupní podmínky | Admin je přihlášen do systému, vytvořil studijní program, předmět a termín |
| Scénáře | Admin zvolí u předmětu možnost smazat <br> Systém smaže data |
| Alternativní scénář – chybné ověření požadavku | Admin odeslal požadavek <br> Systém našel chybu při ověření chybu <br> Systém vrátí admin chybovou hlášku |
| Výstupní podmínky | Admin smaže termín |

| Název případu užití | Přiřadit učitele k termínu |
| ------ | ------ |
| Identifikace | UC32 |
| Cíl | Přiřadit učitele k termínu | 
| Aktéři | Admin |
| Vstupní podmínky | Admin je přihlášen do systému, vytvořil studijní program, předmět a termín |
| Scénáře | Admin zvolí u termínu možnost přiřadit učitele <br> Systém zobrazí adminovi formulář <br> Admin formulář vyplní a odešle <br> Systém formulář ověří <br> Systém uloží data |
| Alternativní scénář – validace formulář | Admin odeslal data <br> Systém našel chybu při validaci dat <br> Systém vrátí admin chybovou hlášku |
| Výstupní podmínky | Admin přiřadí učitele k termínu |

| Název případu užití | Odebrat učitele |
| ------ | ------ |
| Identifikace | UC33 |
| Cíl | Odebrat učitele od termínu | 
| Aktéři | Admin |
| Vstupní podmínky | Admin je přihlášen do systému, vytvořil studijní program, předmět a termín, ke kterému přiřadil učitele |
| Scénáře | Admin zvolí u termínu možnost odebrat učitele <br> Systém odebere termín |
| Alternativní scénář – chybné ověření požadavku | Admin odeslal požadavek <br> Systém našel chybu při ověření chybu <br> Systém vrátí admin chybovou hlášku |
| Výstupní podmínky | Admin odebere učitele od termínu |

## Class diagram

![image](https://www.stagl.cz/imageHosting/vse/s1.png)
![image](https://www.stagl.cz/imageHosting/vse/s2.png)

## Návrh UI

### Přihlašovací stránka

![image](/uploads/a39434cddfb91e7bbe07322c764886f3/image.png)

### Stránka pro admina

![image](/uploads/9a2c376f88e3d140d6325aad12bd4496/image.png)

### Přidání studijního programu

![image](/uploads/7eb17b08718d2a29e76bff09fa229c90/image.png)

### Přidání předmětu

![image](/uploads/a3b8e2b727e7e4c8094c8119a9fec9e0/image.png)

### Přidání studenta

![image](/uploads/f4882ebec51ec9f7686d7c89adbe7a0c/image.png)

### Přidání učitele

![image](/uploads/9b77802a0197706b8e75325a1e1c171c/image.png)

### Stránka pro studenta

![image](/uploads/42375eb41d571ea30876157eb2e69daa/image.png)

### Stránka pro učitele

![image](/uploads/53d7e211d63eadb8ba0fa0711b6e49eb/image.png)

## Struktura databáze

 ![database](/uploads/716a50e35d7dad6e338bc60981eb4030/database.PNG)

## Testovací případy
Jednotkové testování
- Vytvořené jUnit testy pro všechny HDS a jejich alternativní scénáře.
Integrační testy
- Nasazení na prostředí Jenkins. Napsané komplexnější testy pro propojení modulů
Testy použitelnosti
- Růční testování.
- Testuje se plynulost, bezchybnost a intuitivnost.
Instalační testování
 - Pokus zprovoznění systému na více druhů hardwaru či softwaru.
Testy bezpečnost
- Testování toho, zda se uživatelé dostanou tam, kam mají.
